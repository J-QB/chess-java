package com.company;

import java.util.LinkedList;

public class Game {
    private Board board;
    private LinkedList<Move> movesPlayed;
    private LinkedList<Piece> aliviePieces;

    private void initialize(){
        board.setBoard();
        movesPlayed.clear();
        //add all pieces to alivePieces
        for(int i = 0; i < 2; i++){
            for(int j = 0; j < 8; j++){
                aliviePieces.add(board.tab[i][j].getPiece());
                aliviePieces.add(board.tab[i + 6][j].getPiece());
            }
        }
    }
}
