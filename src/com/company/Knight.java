package com.company;

public class Knight extends Piece {

    public Knight(boolean is_white) {
        super(is_white);
        this.setName("K");
    }

    @Override
    public boolean canMove(Board tab, Tile start, Tile end) {
        if(end.getPiece() != null){
            if(end.getPiece().isWhile() == this.isWhile()){
                System.out.println("can't move on your piece");
                return false;
            }
        }

        boolean ok = false;

        int Xmove[] = {2, 1, -1, -2, -2, -1, 1, 2};
        int Ymove[] = {1, 2, 2, 1, -1, -2, -2, -1};

        for(int i = 0; i < 8; i++){
            int x = end.getX() - start.getX();
            int y = end.getY() - start.getY();
            if(x == Xmove[i] && y == Ymove[i]){
                ok = true;
            }
        }

        if(ok){
            if(tab.tab[end.getY()][end.getX()].getPiece() != null){
                System.out.println("Kill");
            }
            return true;
        }
        else{
            System.out.println("Knight doesn't move like that");
            return false;
        }
    }
}
